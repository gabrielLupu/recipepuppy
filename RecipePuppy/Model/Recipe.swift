//
//  Recipe.swift
//  RecipePuppy
//
//  Created by Gabriel Lupu on 21/11/2017.
//  Copyright © 2017 MO2O. All rights reserved.
//

import Foundation
import SwiftyJSON

class Recipe: NSObject {
    
    private(set) var title:String!
    private(set) var href:URL!
    private(set) var ingredients:String!
    private(set) var thumbnail:URL?
    
    init?(json:JSON) {
        self.title = json["title"].stringValue
        self.href = URL(string: json["href"].stringValue)
        self.ingredients = json["ingredients"].stringValue
        self.thumbnail = URL(string: json["thumbnail"].stringValue)
        super.init()
        
        guard !self.title.isEmpty && self.href != nil else { return nil }
    }
}
