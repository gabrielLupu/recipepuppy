//
//  ViewController.swift
//  RecipePuppy
//
//  Created by Gabriel Lupu on 21/11/2017.
//  Copyright © 2017 MO2O. All rights reserved.
//

import UIKit
import AlamofireImage

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    
    static let CELL_ID_RECIPE = "recipeCell"
    static let CELL_ID_LOADING = "loadingCell"
    
    
    private var results = [Recipe]()
    
    private var currentSearch:String = ""
    private var currentPage = 1
    private var isLoading = false
    
    private var searchWorkItem:DispatchWorkItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.reloadData(search: "")
    }
    
    // MARK: - UISearchbarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchWorkItem = self.searchWorkItem {
            searchWorkItem.cancel()
        }
        searchWorkItem = DispatchWorkItem () {
            self.reloadData(search: searchBar.text!)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: self.searchWorkItem!)
    }
    
    //MARK: - Load data
    
    func reloadData(search:String) {
        self.currentSearch = search
        self.currentPage = 1
        self.loadNext()
    }
    
    func loadNext() {
        print("loading page: \(self.currentPage)")
        self.isLoading = true
        DataService.shared.search(recipeName: self.currentSearch, page: currentPage) { results, error in
            self.isLoading = false
            if let results = results, !results.isEmpty {
                if self.currentPage == 1 {
                    //we are resetting
                    self.results.removeAll()
                }
                self.results += results
                self.currentPage += 1
                self.tableView.reloadData()
            } else {
                //treat error
                print("error loading from api: \(error)")
            }
        }
    }

    //MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count + ( isLoading ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < results.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: ViewController.CELL_ID_RECIPE) as! RecipeCell
            cell.data = self.results[indexPath.row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: ViewController.CELL_ID_LOADING)!
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.results.count - 1  && !isLoading && self.currentPage > 1 {
            self.loadNext()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row < self.results.count {
            UIApplication.shared.open(self.results[indexPath.row].href)
        }
    }

}

class RecipeCell: UITableViewCell {
    @IBOutlet weak var thumbnailImage:UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var ingredients:UILabel!
    
    var data:Recipe? {
        didSet {
            if let data = self.data {
                titleView.text = data.title
                ingredients.text = data.ingredients
                thumbnailImage.image = nil
                if let url = data.thumbnail {
                    thumbnailImage.af_setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "default_placeholder"))
                } else {
                    thumbnailImage.image = #imageLiteral(resourceName: "default_placeholder")
                }
            }
        }
    }
}

