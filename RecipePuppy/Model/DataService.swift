//
//  DataService.swift
//  RecipePuppy
//
//  Created by Gabriel Lupu on 21/11/2017.
//  Copyright © 2017 MO2O. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class DataService {
    
    static let shared = DataService()
    
    static let BASE_URL = "http://www.recipepuppy.com/api/"
    
    private func call(endpointUrl url:String, completion: @escaping (JSON?, Error?) -> Void) {
        Alamofire.request(url).responseJSON { response in
            switch response.result {
            case .success(let value):
                completion(JSON(value), nil)
            case .failure(let error):
                completion(nil, error)
            }
        }
    }
    
    
    internal func search(recipeName: String, page: Int = 0,
                completion: @escaping ([Recipe]?, Error?) -> Void) {
        var urlComps = URLComponents(string: DataService.BASE_URL)!
        urlComps.queryItems = [
            URLQueryItem(name: "q", value: recipeName),
            URLQueryItem(name: "p", value: String(page))
        ]
        self.call(endpointUrl: urlComps.url!.absoluteString) { json, error in
            if let json = json,
                let data = json["results"].array?.flatMap( { Recipe(json: $0) }) {
                completion(data, nil)
            } else {
                completion(nil, error)
            }
        }
    }
}
